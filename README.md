## costs-overview

```
docker-compose exec costs-overview sh
```

```
ExcelJs: https://www.npmjs.com/package/exceljs#writing-xlsx
```

```yaml
FROM alpine:3.9

WORKDIR /app

RUN apk add --update nodejs nodejs-npm && \
    npm install -g typescript nodemon

COPY service .

RUN npm install

EXPOSE 9400

CMD [ "nodemon" ]
```
