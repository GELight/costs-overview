import controller from "./controller";

export default (app) => {
    app.get('/', controller.index);
    app.get('/file/:name', controller.getFile);
}
