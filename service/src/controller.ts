// import ServiceConfig from "./service-config";
import * as fs from 'fs';
import * as path from 'path';
import update from './update/update';

// import XlsxPopulate from 'xlsx-populate';
import csv from 'csv-parser';
csv({ separator: ';' });

import Excel from 'exceljs';

export default {

    async getFile (req, res) {
        let name = req.params.name
        let pathToFile = path.join(__dirname, '..', '..', `${name}.xlsx`);

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + `${name}.xlsx`);

        res.download(pathToFile);
    },

    async index (req, res) {

        let csvFilesPath = path.join(__dirname, '..', '..', 'service', 'csv');
        let baseFile = path.join(__dirname, '..', '..', 'xlsx', 'base.xlsx');
        let generatedCsvFilesPath = path.join(__dirname, '..', '..');

        let files = await Promise.resolve(readAllCsvFiles(csvFilesPath));
        if (Object.keys(files).length) {
            
            let workbook = new Excel.Workbook();
            workbook.xlsx.readFile(baseFile).then(async (workbook) => {
                console.log('');
                console.log(`Basefile ${baseFile} opened.`);
                
                for (let year in files) {
                    
                    let updated_workbook = new update(workbook, year);
                    
                    for (let filename in files[year]) {
                        updated_workbook.csv(files[year][filename]);
                    }

                    let csvPath = `${generatedCsvFilesPath}/${year}.xlsx`;
                    await updated_workbook.getWorkbook().xlsx.writeFile(csvPath).then(() => {
                        console.log(`File ${csvPath} written sucessfully.`);
                    });

                }

            });
        }
        
        res.end();
    }

}

let readAllCsvFiles = async (csvPath:any) => {

    let result = {};
    let files = fs.readdirSync(csvPath);

    if (files.length) {
        for (let i=0; i<files.length; i++) {
            let p = `${csvPath}/${files[i]}`;
            
            if (fs.lstatSync(p).isFile()) {
                let filename = path.basename(p);
                result[filename] = await fs.createReadStream(p)
                    .pipe(csv())
                    .on('data', (data) => {
                        return data;
                    })
            }
            
            if (fs.lstatSync(p).isDirectory()) {
                result[files[i]] = await readAllCsvFiles(p);
            }
            
        }
    }
    
    return result;
}