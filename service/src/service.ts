import ServiceConfig from "./service-config";
import express from "express";
import bodyParser from 'body-parser';
import router from "./router";

const app = express();

app.use(express.static(ServiceConfig.uiRootPath));
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

router(app);

app.listen(ServiceConfig.port, function () {
  console.log('Microservice "' + ServiceConfig.name + '" is listening to http://localhost:' + ServiceConfig.port);
});
