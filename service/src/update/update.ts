import { Workbook } from "exceljs";
import { CsvParser } from "csv-parser";
import moment from 'moment';

export default class update {

  workbook: Workbook;
  year: number;
  baseDate: any;

  constructor (workbook:Workbook, year:any) {
    this.workbook = workbook;
    this.year = parseInt(year);
    this.baseDate = new Date(this.year, 0, 1);

    console.log(`Start update workbook for year ${year}.`);

    this.updateBaseDate();
    this.updateSheetDates();
  }

  updateBaseDate () {
    let worksheetOverview = this.workbook.getWorksheet('Kosten');
    worksheetOverview.getCell('C2').value = this.baseDate;

    console.log(`Updated base date to '${this.baseDate}'.`);
  }

  updateSheetDates () {

    let worksheet = this.workbook.getWorksheet('JAN');
    let day = moment(this.baseDate); // baseDate: 2019-01-01T00:00:00.000Z >>> 01.01.2019
    
    let startRow = 12;
    console.log(worksheet.getCell('B'+startRow).value)
    
    worksheet.getCell('B12').value = { formula: 'Jahresanfang', result: this.baseDate, sharedFormula: undefined, date1904: false };

    for (let i=1; i<31; i++) {
      console.log(worksheet.getCell('B'+(12+i)).value, day.add(i, 'd').format("DD.MM.YYYY"));
      worksheet.getCell('B'+(12+i)).value = { formula: 'B12+'+i, result: day.add(i, 'd').format("DD.MM.YYYY"), sharedFormula: 'B14', date1904: false };;
    }

    // for (let i=startRow; i<(startRow + 30); i++) {
    //   console.log(worksheet.getCell('B'+i).value, month.startOf("month").day(i-11).format("DD.MM.YYYY"));
    //   worksheet.getCell('B'+i).value = { formula: '=123.45 + 200', date1904: false };
    // }

    console.log(`Updated sheet dates.`);
  }

  csv (csv:CsvParser) {
    
    // let worksheet = this.workbook.getWorksheet('SEP');
    // worksheet.getCell('E34').value = { formula: '=123.45 + 200', date1904: false };
    
    // console.log(`Updated worksheets for ${this.year} by csv.`);
  }

  getWorkbook () {
    return this.workbook;
  }

}