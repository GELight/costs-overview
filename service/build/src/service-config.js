"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const package_json_1 = __importDefault(require("./../package.json"));
const config = {
    name: package_json_1.default.name,
    version: package_json_1.default.version,
    port: package_json_1.default.server.port,
    uiRootPath: 'frontend/'
};
exports.default = config;
//# sourceMappingURL=service-config.js.map