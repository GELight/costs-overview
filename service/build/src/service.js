"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const service_config_1 = __importDefault(require("./service-config"));
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const router_1 = __importDefault(require("./router"));
const app = express_1.default();
app.use(express_1.default.static(service_config_1.default.uiRootPath));
app.use(body_parser_1.default.urlencoded({ extended: false }));
// parse application/json
app.use(body_parser_1.default.json());
router_1.default(app);
app.listen(service_config_1.default.port, function () {
    console.log('Microservice "' + service_config_1.default.name + '" is listening to http://localhost:' + service_config_1.default.port);
});
//# sourceMappingURL=service.js.map