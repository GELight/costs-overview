"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const service_config_1 = __importDefault(require("./service-config"));
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
exports.default = {
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let uiRootPath = path.join(__dirname, '..', service_config_1.default.uiRootPath);
            fs.readFile(uiRootPath + 'index.html__', 'utf8', (error, data) => {
                res.set('Content-Type', 'text/html');
                var buffer = Buffer.from(data);
                res.send(buffer);
                res.end();
            });
        });
    }
};
//# sourceMappingURL=controller.js.map